package com.pabloteigon.events.domain.extension

import java.text.SimpleDateFormat
import java.util.*

/**
 * Pattern: dd/MM/yyyy às HH:mm
 */
fun Date.formatToViewDateTimeDefaults(): String {
    val sdf = SimpleDateFormat("dd/MM/yyyy 'às' HH:mm", Locale.getDefault())
    return sdf.format(this)
}