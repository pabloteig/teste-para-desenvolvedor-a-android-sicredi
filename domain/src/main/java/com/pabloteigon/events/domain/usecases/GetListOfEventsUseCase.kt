package com.pabloteigon.events.domain.usecases

import com.pabloteigon.events.domain.entities.Event
import com.pabloteigon.events.domain.repository.EventsRepository
import io.reactivex.Scheduler
import io.reactivex.Single

interface GetListOfEventsUseCase {
    fun execute(): Single<List<Event>>
}

class GetListOfEventsUseCaseImpl(
    private val eventsRepository: EventsRepository,
    private val scheduler: Scheduler
) : GetListOfEventsUseCase {

    override fun execute(

    ): Single<List<Event>> {
        return eventsRepository.getListOfEvents()
            .subscribeOn(scheduler)
    }
}