package com.pabloteigon.events.domain.usecases

import com.pabloteigon.events.domain.entities.Event
import com.pabloteigon.events.domain.repository.EventsRepository
import io.reactivex.Scheduler
import io.reactivex.Single

interface GetEventByIdUseCase {
    fun execute(eventId: String): Single<Event>
}

class GetEventByIdUseCaseImpl(
    private val eventsRepository: EventsRepository,
    private val scheduler: Scheduler
) : GetEventByIdUseCase {

    override fun execute(
        eventId: String
    ): Single<Event> {
        return eventsRepository.getEventById(eventId)
            .subscribeOn(scheduler)
    }
}