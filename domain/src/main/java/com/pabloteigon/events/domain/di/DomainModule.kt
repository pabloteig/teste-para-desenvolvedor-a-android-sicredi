package com.pabloteigon.events.domain.di

import com.pabloteigon.events.domain.usecases.*
import io.reactivex.schedulers.Schedulers
import org.koin.dsl.module

val useCaseModule = module {

    factory<GetListOfEventsUseCase> {
        GetListOfEventsUseCaseImpl(
            eventsRepository = get(),
            scheduler = Schedulers.io()
        )
    }

    factory<GetEventByIdUseCase> {
        GetEventByIdUseCaseImpl(
            eventsRepository = get(),
            scheduler = Schedulers.io()
        )
    }

    factory<CheckInUseCase> {
        CheckInUseCaseImpl(
            eventsRepository = get(),
            scheduler = Schedulers.io()
        )
    }
}

val domainModule = listOf(useCaseModule)