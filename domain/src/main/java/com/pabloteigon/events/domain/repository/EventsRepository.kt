package com.pabloteigon.events.domain.repository

import com.pabloteigon.events.domain.entities.Event
import io.reactivex.Completable
import io.reactivex.Single

interface EventsRepository {

    fun getListOfEvents(): Single<List<Event>>

    fun getEventById(eventId: String): Single<Event>

    fun checkIn(body: Map<String, Any>): Completable
}