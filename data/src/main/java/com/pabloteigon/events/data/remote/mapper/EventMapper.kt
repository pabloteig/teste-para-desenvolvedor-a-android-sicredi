package com.pabloteigon.events.data.remote.mapper

import com.pabloteigon.events.data.remote.model.CuponPayload
import com.pabloteigon.events.data.remote.model.EventPayload
import com.pabloteigon.events.data.remote.model.PeoplePayload
import com.pabloteigon.events.domain.entities.Cupon
import com.pabloteigon.events.domain.entities.Event
import com.pabloteigon.events.domain.entities.People
import java.util.*

object EventMapper {

    fun mapList(payload: List<EventPayload>) = payload.map { event(it) }

    fun map(payload: EventPayload) = event(payload)

    private fun event(payload: EventPayload) = Event(
        people = payload.people?.map { people(it) },
        date = getDateTime(payload.date!!),
        description = payload.description,
        image = payload.image,
        longitude = payload.longitude,
        latitude = payload.latitude,
        price = payload.price,
        title = payload.title,
        id = payload.id,
        cupons = payload.cupons?.map { cupon(it) }
    )

    private fun people(payload: PeoplePayload) = People(
        id = payload.id,
        eventId = payload.eventId,
        name = payload.name,
        picture = payload.picture
    )

    private fun cupon(payload: CuponPayload) = Cupon(
        id = payload.id,
        eventId = payload.eventId,
        discount = payload.discount
    )

    private fun getDateTime(s: String): Date? {
        return if (s.isNotEmpty()) {
            Date(s.toLong())
        } else {
            null
        }
    }

}