package com.pabloteigon.events.data.remote.api

import com.pabloteigon.events.data.remote.model.EventPayload
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ServerApi {

    @GET("/api/events")
    fun fetchListOfEvents(
    ): Single<List<EventPayload>>

    @GET("/api/events/{eventId}")
    fun fetchEventById(
        @Path("eventId") eventId: String
    ): Single<EventPayload>

    @POST(" /api/checkin")
    fun checkIn(
        @Body body: Map<String, @JvmSuppressWildcards Any>
    ): Completable

}