package com.pabloteigon.events.data.repository

import com.pabloteigon.events.data.remote.source.RemoteDataSource
import com.pabloteigon.events.domain.entities.Event
import com.pabloteigon.events.domain.repository.EventsRepository
import io.reactivex.Completable
import io.reactivex.Single

class EventsRepositoryImpl(
    private val remoteDataSource: RemoteDataSource
) : EventsRepository {

    override fun getListOfEvents(
    ): Single<List<Event>> {
        return getListOfEventsRemote()
    }

    override fun getEventById(eventId: String): Single<Event> {
        return getEventByIdRemote(eventId)
    }

    override fun checkIn(body: Map<String, Any>): Completable {
        return remoteDataSource.checkIn(body)
    }

    private fun getListOfEventsRemote(
    ): Single<List<Event>> {
        return remoteDataSource.getListOfEvents()
            .flatMap { response ->
                Single.just(response)
            }
    }

    private fun getEventByIdRemote(
        eventId: String
    ): Single<Event> {
        return remoteDataSource.getEventById(eventId)
            .flatMap { response ->
                Single.just(response)
            }
    }

}