package com.pabloteigon.events.data.remote.model

import java.math.BigDecimal

data class EventPayload(
    val people: List<PeoplePayload>?,
    val date: String?,
    val description: String?,
    val image: String?,
    val longitude: String?,
    val latitude: String?,
    val price: BigDecimal?,
    val title: String?,
    val id: String?,
    val cupons: List<CuponPayload>?
)

data class PeoplePayload(
    val id: String?,
    val eventId: String?,
    val name: String?,
    val picture: String?
)

data class CuponPayload(
    val id: String?,
    val eventId: String?,
    val discount: String?
)