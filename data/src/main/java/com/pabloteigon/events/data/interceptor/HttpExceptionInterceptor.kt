package com.pabloteigon.events.data.interceptor

import com.pabloteigon.events.data.exception.AccessDeniedException
import com.pabloteigon.events.data.exception.BadRequestException
import com.pabloteigon.events.data.exception.ConflictException
import com.pabloteigon.events.data.exception.UnauthorizedException
import okhttp3.Interceptor
import okhttp3.Response

class HttpExceptionInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())
        if (response.code() == 400) {
            throw BadRequestException(response.message())
        }
        if (response.code() == 401) {
            throw UnauthorizedException(response.message())
        }
        if (response.code() == 403) {
            throw AccessDeniedException(response.message())
        }
        if (response.code() == 409) {
            throw ConflictException()
        }

        return response
    }
}