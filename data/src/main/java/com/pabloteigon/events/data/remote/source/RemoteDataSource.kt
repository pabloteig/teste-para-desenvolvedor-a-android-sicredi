package com.pabloteigon.events.data.remote.source

import com.pabloteigon.events.domain.entities.Event
import io.reactivex.Completable
import io.reactivex.Single

interface RemoteDataSource {

    fun getListOfEvents(): Single<List<Event>>

    fun getEventById(eventId: String): Single<Event>

    fun checkIn(body: Map<String, Any>): Completable

}