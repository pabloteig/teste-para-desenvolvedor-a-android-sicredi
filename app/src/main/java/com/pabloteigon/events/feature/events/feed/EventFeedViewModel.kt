package com.pabloteigon.events.feature.events.feed

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import com.pabloteigon.events.domain.entities.Event
import com.pabloteigon.events.domain.usecases.GetListOfEventsUseCase
import com.pabloteigon.events.feature.viewmodel.BaseViewModel
import com.pabloteigon.events.feature.viewmodel.StateMachineSingle
import com.pabloteigon.events.feature.viewmodel.ViewState
import io.reactivex.Scheduler
import io.reactivex.rxkotlin.plusAssign

class EventFeedViewModel(
    val getListOfEventsUseCase: GetListOfEventsUseCase,
    val uiScheduler: Scheduler
) : BaseViewModel(), LifecycleObserver {

    val events = MutableLiveData<List<Event>>().apply { value = emptyList() }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onStart() {
        getEvents()
    }

    private fun getEvents() {
        loadingVisibility.value = true
        disposables += getListOfEventsUseCase.execute()
            .compose(StateMachineSingle())
            .observeOn(uiScheduler)
            .subscribe(
                {
                    if (it is ViewState.Success) {
                        events.value = it.data

                    } else if (it is ViewState.Failed) {
                        errorState.value = ViewState.Failed(it.throwable)
                    }
                    loadingVisibility.value = false
                },
                {
                    loadingVisibility.value = false
                    errorState.value = ViewState.Failed(it)
                }
            )
    }
}