package com.pabloteigon.events.feature.events.feed

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.ViewCompat
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.RecyclerView
import com.pabloteigon.events.R
import com.pabloteigon.events.databinding.AdapterCharactersItemBinding
import com.pabloteigon.events.domain.entities.Event
import com.squareup.picasso.Picasso

class EventsAdapter : RecyclerView.Adapter<EventsAdapter.ViewHolder>() {

    private var events: List<Event> = emptyList()

    companion object {
        @JvmStatic
        @BindingAdapter("events")
        fun RecyclerView.bindItems(items: List<Event>) {
            val adapter = adapter as EventsAdapter
            adapter.update(items)
        }

        @JvmStatic
        @BindingAdapter("bind:imageUrl")
        fun loadImage(view: ImageView, imageUrl: String?) {
            if (!imageUrl.isNullOrEmpty()) {
                Picasso.get().load(imageUrl).centerCrop().fit()
                    .placeholder(R.drawable.progress_animation)
                    .error(R.drawable.ic_image_24dp)
                    .into(view)
            }
        }
    }

    fun update(items: List<Event>) {
        this.events = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.adapter_characters_item, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        (events[position]).let { character ->
            with(holder) {
                itemView.tag = character
                bind(createOnClickListener(binding, character), character)
            }
        }
    }

    private fun createOnClickListener(
        binding: AdapterCharactersItemBinding,
        event: Event
    ): View.OnClickListener {
        return View.OnClickListener {
            val eventId = event.id

            val directions = MainFragmentDirections.viewCharacterDetails(
                eventId = eventId!!
            )

            val extras = FragmentNavigatorExtras(
                binding.tvEventTitle to "name_${eventId}",
                binding.tvEventDescription to "description${eventId}",
                binding.ivEventImg to "thumbnail_${eventId}"
            )
            it.findNavController().navigate(directions, extras)
        }
    }

    class ViewHolder(val binding: AdapterCharactersItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(listener: View.OnClickListener, value: Event) {
            ViewCompat.setTransitionName(binding.tvEventTitle, "title_${value.id}")
            ViewCompat.setTransitionName(binding.tvEventDescription, "description${value.id}")
            ViewCompat.setTransitionName(binding.ivEventImg, "thumbnail_${value.id}")
            with(binding) {
                this.event = value
                executePendingBindings()
            }
            binding.root.setOnClickListener(listener)
        }
    }

    override fun getItemCount(): Int = events.size
}