package com.pabloteigon.events.feature.events.feed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.pabloteigon.events.R
import com.pabloteigon.events.databinding.MainFragmentBinding
import com.pabloteigon.events.feature.AbstractFragment
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : AbstractFragment() {

    override val viewModel: EventFeedViewModel by viewModel()
    private val eventsAdapter: EventsAdapter by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        this.lifecycle.addObserver(viewModel)

        val binding = DataBindingUtil.inflate<MainFragmentBinding>(
            inflater, R.layout.main_fragment, container, false
        ).apply {
            vm = viewModel
            lifecycleOwner = this@MainFragment
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupErrorState(clEventsFragmentContainer)

        rvEventsList.apply {
            this.adapter = eventsAdapter
            postponeEnterTransition()
            viewTreeObserver
                .addOnPreDrawListener {
                    startPostponedEnterTransition()
                    true
                }
        }
    }
}
