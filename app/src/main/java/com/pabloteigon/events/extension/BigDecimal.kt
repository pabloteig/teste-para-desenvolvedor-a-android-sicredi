package com.pabloteigon.events.extension

import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.*

fun BigDecimal.formatForBrazilianCurrency(): String {
    val locale = Locale.getDefault()
    val brazilianFormat = DecimalFormat.getCurrencyInstance(locale)
    val symbol: String = Currency.getInstance(locale).getSymbol(locale)

    return brazilianFormat.format(this).replace(symbol, "$symbol ")
}