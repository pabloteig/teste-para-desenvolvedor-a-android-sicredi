package com.pabloteigon.events

import android.app.Application
import com.pabloteigon.events.data.di.dataModules
import com.pabloteigon.events.di.presentationModule
import com.pabloteigon.events.domain.di.domainModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class EventsAplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@EventsAplication)

            modules(domainModule + dataModules + listOf(presentationModule))
        }
    }
}