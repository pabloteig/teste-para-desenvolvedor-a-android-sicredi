package com.pabloteigon.events.repository

import com.pabloteigon.events.base.BaseTest
import com.pabloteigon.events.data.di.dataModules
import com.pabloteigon.events.domain.entities.Event
import com.pabloteigon.events.domain.repository.EventsRepository
import com.pabloteigon.events.feature.viewmodel.StateMachineCompletable
import com.pabloteigon.events.feature.viewmodel.StateMachineSingle
import com.pabloteigon.events.feature.viewmodel.ViewState
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.core.context.startKoin
import org.koin.test.inject

@RunWith(JUnit4::class)
class EventsRepositoryTest : BaseTest() {

    //Target
    private val mRepo: EventsRepository by inject()

    private val mEventsSize = 3
    private val mEventId = "1"
    private val mThrowableMessage = "Bad Request"

    @Before
    fun start() {
        super.setUp()

        startKoin { modules(dataModules) }
    }

    @Test
    fun test_list_of_events_retrieves_expected_data() = runBlocking {
        var events: List<Event> = emptyList()
        val dataReceived = mRepo.getListOfEvents()
            .compose(StateMachineSingle())
            .subscribe(
                {
                    if (it is ViewState.Success) {
                        events = it.data
                    }
                },
                {

                }
            )
        assertNotNull(dataReceived)
        assertEquals(mEventsSize, events.size)
    }

    @Test
    fun test_event_by_id_retrieves_expected_data() = runBlocking {
        var event = Event()
        val dataReceived = mRepo.getEventById(mEventId)
            .compose(StateMachineSingle())
            .subscribe(
                {
                    if (it is ViewState.Success) {
                        event = it.data
                    }
                },
                {

                }
            )
        assertNotNull(dataReceived)
        assertEquals(mEventId, event.id)
    }

    @Test
    fun test_check_in_retrieves_expected_data() = runBlocking {
        var error = false
        var throwable = Throwable()
        val dataReceived = mRepo.checkIn(getBody())
            .compose(StateMachineCompletable())
            .subscribe(
                {
                    error = false
                },
                {
                    error = true
                    throwable = it
                }
            )
        assertNotNull(dataReceived)
        assert(error)
        assertEquals(mThrowableMessage, throwable.message)
    }

    private fun getBody(): HashMap<String, Any> {
        val body = HashMap<String, Any>()

        body["eventId"] = mEventId
        body["name"] = "teste"
        body["email"] = "teste@teste.com"

        return body
    }
}